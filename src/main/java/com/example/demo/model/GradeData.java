package com.example.demo.model;

import java.time.LocalDateTime;


public class GradeData {

    private Long id;

    private PrzedmiotData przedmiot;

    private double ocena;

    private LocalDateTime dataDodania;

    private StudentData student;

    public GradeData(Long id, PrzedmiotData przedmiot, double ocena, LocalDateTime dataDodania, StudentData student) {

        this.id = id;
        this.przedmiot = przedmiot;
        this.ocena = ocena;
        this.dataDodania = dataDodania;
        this.student = student;
    }


    public GradeData(PrzedmiotData przedmiot, double ocena) {
        this.przedmiot = przedmiot;
        this.ocena = ocena;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public PrzedmiotData getPrzedmiot() {

        return przedmiot;
    }

    public void setPrzedmiot(PrzedmiotData przedmiot) {

        this.przedmiot = przedmiot;
    }

    public double getOcena() {

        return ocena;
    }

    public void setOcena(double ocena) {

        this.ocena = ocena;
    }

    public LocalDateTime getDataDodania() {

        return dataDodania;
    }

    public void setDataDodania(LocalDateTime dataDodania) {

        this.dataDodania = dataDodania;
    }

    public StudentData getStudent() {

        return student;
    }

    public void setStudent(StudentData student) {

        this.student = student;
    }

    public GradeData() {

    }

}

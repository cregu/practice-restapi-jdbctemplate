package com.example.demo.model;

public enum PrzedmiotData {
    J_POLSKI("Język Polski"),
    J_ANGIELSKI ("Język Angielski"),
    INFORMATYKA ("Języki programowania"),
    MATEMATYKA (" Matematyka"),
    RELIGIA ("Religia");

    private String opis;

    PrzedmiotData(String opis) {
        this.opis = opis;
    }

    public String getOpis() {
        return opis;
    }
}

package com.example.demo.services;

import com.example.demo.model.StudentData;

import java.util.List;
import java.util.Optional;


public interface StudentsRestService {

    Optional<StudentData> getStudentById(String studentId);

    List<StudentData> getStudents();

    boolean deleteStudent(String studentId);

    void addStudent(StudentData studentData);

    void updateStudent(String studentId, StudentData updatedStudent);

}

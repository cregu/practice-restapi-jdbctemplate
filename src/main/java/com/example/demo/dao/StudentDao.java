package com.example.demo.dao;

import com.example.demo.model.StudentData;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface StudentDao {

    Optional<StudentData> getStudentById(String studentId);

    List<Map<String, Object>> getStudents();

    boolean deleteStudent(String studentId);

    void addStudent(StudentData studentData);

    void updateStudent(String studentId, StudentData updatedStudent);

}

package com.example.demo.dao;

import com.example.demo.model.StudentData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class StudentDaoImpl implements StudentDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //TODO: implement the rest of the methods using jdbcTemplate

    @Override
    public Optional<StudentData> getStudentById(String studentId) {
        return Optional.empty();
    }

    @Override
    public List<Map<String, Object>> getStudents() {
        String sql = "SELECT * FROM student";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public boolean deleteStudent(String studentId) {
        return false;
    }

    @Override
    public void addStudent(StudentData studentData) {

    }

    @Override
    public void updateStudent(String studentId, StudentData updatedStudent) {

    }
}

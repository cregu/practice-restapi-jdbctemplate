package com.example.demo.rest;

import com.example.demo.dao.StudentDaoImpl;
import com.example.demo.model.StudentData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/")
public class StudentDaoController {

    @Autowired
    private StudentDaoImpl studentDaoImpl;

    @GetMapping("/students")
    public ResponseEntity<List<StudentData>> getStudentList(){
        return new ResponseEntity(studentDaoImpl.getStudents(), HttpStatus.OK);

//        return defaultRestStudentService.getStudents();
    }

    @GetMapping("/students/{studentId}")
    public ResponseEntity<StudentData> getStudentById(@PathVariable String studentId) {
        Optional<StudentData> studentOptional = studentDaoImpl.getStudentById(studentId);

        if (studentOptional.isPresent()) {
            return new ResponseEntity<>(studentOptional.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

//        return studentOptional.map(studentData -> new ResponseEntity<>(studentData, HttpStatus.OK).orElse)
    }

    @PostMapping("/students")
//    public ResponseEntity<StudentData> addStudent(@RequestBody StudentData studentData){
//        return new ResponseEntity<StudentData>(defaultRestStudentService.addStudent(studentData), HttpStatus.OK);
    public void addStudent(@RequestBody StudentData studentData) {
        studentDaoImpl.addStudent(studentData);
    }

    @PutMapping("/students/{studentId}")
//    @ResponseBody
    public void updateStudent(@PathVariable String studentId, @RequestBody StudentData updatedStudent){
        studentDaoImpl.updateStudent(studentId,updatedStudent);
    }

    @DeleteMapping("/students/{studentId}")
    public boolean deleteStudent(@PathVariable String studentId){
        return studentDaoImpl.deleteStudent(studentId);
    }



}

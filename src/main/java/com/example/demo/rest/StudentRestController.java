package com.example.demo.rest;

import com.example.demo.model.StudentData;
import com.example.demo.services.DefaultRestStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//@RestController
//@RequestMapping(path = "/")
public class StudentRestController {

    @Autowired
    private DefaultRestStudentService defaultRestStudentService;

    @GetMapping("/students")
    public ResponseEntity<List<StudentData>> getStudentList(){
        return new ResponseEntity(defaultRestStudentService.getStudents(), HttpStatus.OK);

//        return defaultRestStudentService.getStudents();
    }

    @GetMapping("/students/{studentId}")
    public ResponseEntity<StudentData> getStudentById(@PathVariable String studentId) {
        Optional<StudentData> studentOptional = defaultRestStudentService.getStudentById(studentId);

        if (studentOptional.isPresent()) {
            return new ResponseEntity<>(studentOptional.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

//        return studentOptional.map(studentData -> new ResponseEntity<>(studentData, HttpStatus.OK).orElse)
    }

    @PostMapping("/students")
//    public ResponseEntity<StudentData> addStudent(@RequestBody StudentData studentData){
//        return new ResponseEntity<StudentData>(defaultRestStudentService.addStudent(studentData), HttpStatus.OK);
    public void addStudent(@RequestBody StudentData studentData) {
        defaultRestStudentService.addStudent(studentData);
    }

    @PutMapping("/students/{studentId}")
//    @ResponseBody
    public void updateStudent(@PathVariable String studentId, @RequestBody StudentData updatedStudent){
        defaultRestStudentService.updateStudent(studentId,updatedStudent);
    }

    @DeleteMapping("/students/{studentId}")
    public boolean deleteStudent(@PathVariable String studentId){
        return defaultRestStudentService.deleteStudent(studentId);
    }

}
